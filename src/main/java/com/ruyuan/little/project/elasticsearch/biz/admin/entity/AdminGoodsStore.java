package com.ruyuan.little.project.elasticsearch.biz.admin.entity;

import com.ruyuan.little.project.elasticsearch.biz.common.dto.GoodsRelationField;
import lombok.Data;

import java.util.List;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:后台店铺实体
 **/
@Data
public class AdminGoodsStore {

    /**
     * 主键id
     */
    private String id;

    /**
     * 店铺名称
     */
    private String storeName;

    /**
     * 店铺简介
     */
    private String storeIntroduction;

    /**
     * 店铺品牌
     */
    private String storeBrand;

    /**
     * 店铺标签
     */
    private List<String> storeTags;

    /**
     * 店铺开店时间
     */
    private String openDate;

    /**
     * 关联关系字段
     */
    private String goodsRelationField = GoodsRelationField.GOODS_STORE;
}
